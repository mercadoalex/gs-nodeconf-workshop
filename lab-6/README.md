#Lab 6 - Canary rollout and mirroring

This task shows you how to gradually migrate traffic from one version of a microservice to another. For example, you might migrate traffic from an older version to a new version.

A common use case is to see if any unexpected spikes in errors, latency or saturation of CPU, Memory, Disc or Network IO occur. In Istio, you accomplish this goal by configuring a sequence of rules that route a percentage of traffic to one service or another. In this task, you will send 30% of traffic to new version v.0.0.53 and 70% to old version. Then, you will complete the migration by sending 100% of traffic to new version.

### Part 1 - Canary rollout
The following are the objects ready to apply on your cluster, just set your YOUR_PROJECT_ID and your versions (tags):

Active version (`0.0.52 NOTE: This is the tag you cut before`)

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: view-cache-active
  labels:
    app: view-cache
    version: v0.0.52
spec:
  replicas: 2
  selector:
    matchLabels:
      app: view-cache
      version: v0.0.52
  template:
    metadata:
      labels:
        app: view-cache
        version: v0.0.52
    spec:
      containers:
      - name: view-cache
        image: "us.gcr.io/{YOUR_PROJECT_ID}/view-cache:0.0.52"
        ports:
        - containerPort: 4000
```

Canary version (`0.0.53`)
```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: view-cache-canary
  labels:
    app: view-cache
    version: v0.0.53
spec:
  replicas: 2
  selector:
    matchLabels:
      app: view-cache
      version: v0.0.53
  template:
    metadata:
      labels:
        app: view-cache
        version: v0.0.53
    spec:
      containers:
      - name: view-cache
        image: "us.gcr.io/{YOUR_PROJECT_ID}/view-cache:0.0.53"
        ports:
        - containerPort: 4000
```
Service
```yaml
apiVersion: v1
kind: Service
metadata:
  name: view-cache-svc
  labels:
    app: view-cache
spec:
  ports:
  - port: 4000
    name: http
  selector:
    app: view-cache
```

DestinationRule
```yaml
apiVersion: networking.istio.io/v1alpha3
kind: DestinationRule
metadata:
  name: view-cache
spec:
  host: view-cache-svc
  subsets:
  - labels:
      version: v0.0.52
    name: active
  - name: canary
    labels:
      version: v0.0.53
```

VirtualService
```yaml
apiVersion: networking.istio.io/v1alpha3
kind: VirtualService
metadata:
  name: view-cache
spec:
  hosts:
  - '*'
  gateways:
  - view-cache-gateway
  http:
  - match:
    - uri:
        prefix: /get-data
    route:
    - destination:
        host: view-cache-svc
        subset: active
      weight: 70
    - destination:
        host: view-cache-svc
        subset: canary
      weight: 30
```
Gateway
```yaml
apiVersion: networking.istio.io/v1alpha3
kind: Gateway
metadata:
  name: view-cache-gateway
spec:
  selector:
    istio: ingressgateway # use istio default controller
  servers:
  - port:
      number: 80
      name: http
      protocol: HTTP
    hosts:
    - "*"
```
Note: Pay atention to `v` character at the beginning of some values, e.g. `v0.0.52`   
### Part 2 - Mirroring
Traffic mirroring, also called shadowing, is a powerful concept that allows feature teams to bring changes to production with as little risk as possible. Mirroring sends a copy of live traffic to a mirrored service. The mirrored traffic happens out of band of the critical request path for the primary service.

Mirror version (`0.0.54`)
```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: view-cache-mirrored
  labels:
    app: view-cache
    version: v0.0.54
spec:
  replicas: 2
  selector:
    matchLabels:
      app: view-cache
      version: v0.0.54
  template:
    metadata:
      labels:
        app: view-cache
        version: v0.0.54
    spec:
      containers:
      - name: view-cache
        image: "us.gcr.io/{YOUR_PROJECT_ID}/view-cache:0.0.54"
        ports:
        - containerPort: 4000
```
DestinationRule
```yaml
apiVersion: networking.istio.io/v1alpha3
kind: DestinationRule
metadata:
  name: view-cache
spec:
  host: view-cache-svc
  subsets:
  - labels:
      version: v0.0.52
    name: active
  - labels:
      version: v0.0.53
    name: canary
  - name: mirrored
    labels:
      version: v0.0.54
```
VirtualService
```yaml
apiVersion: networking.istio.io/v1alpha3
kind: VirtualService
metadata:
  name: view-cache
spec:
  hosts:
  - '*'
  gateways:
  - view-cache-gateway
  http:
  - match:
    - uri:
        prefix: /get-data
    route:
    - destination:
        host: view-cache-svc
        subset: active
      weight: 70
    - destination:
        host: view-cache-svc
        subset: canary
      weight: 30
    mirror:
      host: view-cache-svc
      subset: mirrored
```
Note: Pay atention to `v` character at the beginning of some values, e.g. `v0.0.54` 
### Part 3 - Applying
#### 3.1 Manual
You can create a file for every object or just put all together and finally apply it into your k8s cluster using:

```kubectl apply -f your_file.yaml```

#### 3.2 Automatically

We need to add the following templates into your `services/view-cache` directory:

[DestinationRule](destination-rule.yaml)
```yaml
apiVersion: networking.istio.io/v1alpha3
kind: DestinationRule
metadata:
  name: view-cache
spec:
  host: view-cache-svc
  subsets:
  - name: ~ # Will be replaced on runtime  
    labels:
      version: ~ # Will be replaced on runtime

```
[VirtualService](virtual-service.yaml)
```yaml
apiVersion: networking.istio.io/v1alpha3
kind: VirtualService
metadata:
  name: view-cache
spec:
  hosts:
  - "*"
  gateways:
  - view-cache-gateway
  http:
  - match:
    - uri:
        prefix: /get-data
    route:
      - destination:
          host: view-cache-svc
          subset: ~ # Will be replaced on runtime
          # port:
          #   number: 4000 # If a service exposes only a single port it is not required to explicitly select the port.
        weight: 100
```
[Deployment and Service](view-cache-deployment.yaml)
```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: view-cache-active
  labels:
    app: view-cache
    version: VERSION_LABEL
spec:
  replicas: 2
  selector:
    matchLabels:
      app: view-cache
      version: VERSION_LABEL
  template:
    metadata:
      labels:
        app: view-cache
        version: VERSION_LABEL
    spec:
      containers:
      - name: view-cache
        image: "FULL_REPOSITORY_NAME"
        ports:
        - containerPort: 4000
---
apiVersion: v1
kind: Service
metadata:
  name: view-cache-svc
  labels:
    app: view-cache
spec:
  ports:
  - port: 4000
    name: http
  selector:
    app: view-cache
```
[Gateway](view-cache-gateway.yaml)
```yaml
apiVersion: networking.istio.io/v1alpha3
kind: Gateway
metadata:
  name: view-cache-gateway
spec:
  selector:
    istio: ingressgateway # use istio default controller
  servers:
  - port:
      number: 80
      name: http
      protocol: HTTP
    hosts:
    - "*"
```


And add the following [jobs](jobs.yml) into your ``.gitlab.yml`` file:
What we are doing here is 
- getting yq - a basg based json query tool
- logging in to gcloud and installing kubectl
= 
```yaml

.deploy-job: &deploy-job
  stage: deploy
  image: google/cloud-sdk:alpine
  when: manual
  before_script:
    - wget -O /usr/local/bin/jq https://github.com/stedolan/jq/releases/download/jq-1.6/jq-linux64 && chmod +x /usr/local/bin/jq
    - apk add --update py-pip
    - pip install yq
    - echo $GCLOUD_SERVICE_KEY > ${HOME}/gcloud-service-key.json
    - gcloud auth activate-service-account --key-file ${HOME}/gcloud-service-key.json
    - VERSION=$(echo $CI_COMMIT_REF_NAME | awk -F"_" '{print $1}')
    - gcloud config set project ${GCLOUD_PROJECT_ID}
    - gcloud components install kubectl
    - gcloud container clusters get-credentials ${GCLOUD_CLUSTER_NAME} --zone us-central1-a --project ${GCLOUD_PROJECT_ID}
  script:
    - APP="$(basename ${APP_PATH})"
    - cd ${APP_PATH}
    - sed -i "s|FULL_REPOSITORY_NAME|us.gcr.io/${GCLOUD_PROJECT_ID}/${APP}:${VERSION}|" ${APP}-deployment.yaml
    - ${CI_PROJECT_DIR}/scripts/canary.sh ${APP} ${VERSION} ${NAMESPACE} ${WEIGHT}
    - kubectl apply -f k8s --namespace ${NAMESPACE}
  only:
    - tags
  artifacts:
    name: ${CI_JOB_ID}
    paths:
      - ${APP_PATH}/k8s/*.yaml

.mirror-job: &mirror-job
  stage: deploy
  image: google/cloud-sdk:alpine
  when: manual
  before_script:
    - wget -O /usr/local/bin/jq https://github.com/stedolan/jq/releases/download/jq-1.6/jq-linux64 && chmod +x /usr/local/bin/jq
    - apk add --update py-pip
    - pip install yq
    - echo $GCLOUD_SERVICE_KEY > ${HOME}/gcloud-service-key.json
    - gcloud auth activate-service-account --key-file ${HOME}/gcloud-service-key.json
    - VERSION=$(echo $CI_COMMIT_REF_NAME | awk -F"_" '{print $1}')
    - gcloud config set project ${GCLOUD_PROJECT_ID}
    - gcloud components install kubectl
    - gcloud container clusters get-credentials ${GCLOUD_CLUSTER_NAME} --zone us-central1-a --project ${GCLOUD_PROJECT_ID}
  script:
    - APP="$(basename ${APP_PATH})"
    - cd ${APP_PATH}
    - sed -i "s|FULL_REPOSITORY_NAME|us.gcr.io/${GCLOUD_PROJECT_ID}/${APP}:${VERSION}|" ${APP}-deployment.yaml
    - ${CI_PROJECT_DIR}/scripts/mirror.sh ${APP} ${VERSION} ${NAMESPACE}
    - kubectl apply -f k8s --namespace ${NAMESPACE}
  only:
  - tags
  artifacts:
    name: ${CI_JOB_ID}
    paths:
      - ${APP_PATH}/k8s/*.yaml

deploy-cache 30/100 %:
  variables:
    APP_PATH: ./services/view-cache
    WEIGHT: 30
    NAMESPACE: default
  <<: [*deploy-job]

deploy-cache 100/100 %:
  variables:
    APP_PATH: ./services/view-cache
    WEIGHT: 100
    NAMESPACE: default
  <<: [*deploy-job]

deploy-cache-mirror:
  variables:
    APP_PATH: ./services/view-cache
    NAMESPACE: default
  <<: [*mirror-job]
```
Pay atention to last 3 jobs:
* `deploy-cache 30/100 %`
* `deploy-cache 100/100 %`
* `deploy-cache-mirror`

Push your changes, create new tags (3) and verify in the next section:

### Part 4 - Verify

For every tag created in the previous job, run release job.

* 1st tag for active deployment e.g. 0.0.52
![alt text](active.png "Active")
* 2nd tag for canary deployment e.g. 0.0.53
![alt text](canary.png "Canary")
* 3rd tag for mirror deployment e.g. 0.0.54
![alt text](mirror.png "Mirror")

#### Deploy

* Run `deploy-cache 100/100 %` job
![alt text](100.png "deploy-cache 100/100 %")
* Verify 

    ![alt text](100-ok.png "deploy-cache 100/100 %")
* Using your load balancer ip send some request using `get-data` resource e.g.
    ![alt text](get-data.png "")
* Enable on Kiali:
    Versioned

    ![alt text](versioned.png "Versioned")

    Labels

    ![alt text](labels.png "Labels")

    Display

    ![alt text](display.png "Display")

    Time

    ![alt text](time.png "Time")
* Service Mesh
![alt text](graph-active.png "Active deploy")
* Run `deploy-cache 30/100 %` job
![alt text](30.png "deploy-cache 30/100 %")
* Once finished send some requests using `get-data` resource again and verify
![alt text](graph-canary.png "deploy-cache 30/100 %")
* Run `deploy-cache-mirror` job
![alt text](mirror-job.png "deploy-cache 30/100 %")
* Once finished send some requests using `get-data` resource again and verify
![alt text](graph-mirror.png "deploy-cache-mirror")
As you can see, the version 0.0.54 is sending traffic to backend.
You can change the labels and choose `Requests per second` to verify mirroring
![alt text](rps.png "Requests per second")
e.g. `2.18` Request received and `4.36` (2.18 + 0.69 + 1.49 ) sent to backend service

## Concluding Lab 6

You are now finished with Lab 6, in this lab we learned
* How to use traffic shifting 
* How to use mirroring

All done
